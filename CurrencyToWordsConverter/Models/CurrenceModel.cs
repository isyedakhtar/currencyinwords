﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CurrencyToWordsConverter.Models
{
    /// <summary>
    /// Used to receive input from the user
    /// </summary>
    public class CurrencyModel
    {        
        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        [Required]
        [Range(1, 999999999999)]
        public decimal Currency { get; set; }
    }
}