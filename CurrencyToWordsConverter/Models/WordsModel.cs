﻿namespace CurrencyToWordsConverter.Models
{
    /// <summary>
    /// Used to provide output to the user
    /// </summary>
    public class WordsCurrencyModel
    {
        public string CurrencyInWords { get; set; }
        public string Name { get; set; }
    }
}