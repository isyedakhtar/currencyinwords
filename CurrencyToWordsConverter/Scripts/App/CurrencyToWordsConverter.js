﻿$(document).ready(function () {
    $('form').submit(function () {
        $.ajax({
            url: 'http://localhost:26297/api/converter',
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({
                Name: $('#name').val(),
                Currency: $('#currency').val()
            }),
            success: function (result) {
                $("#outputName").text(result.Name);
                $("#outputCurrency").text(result.CurrencyInWords);
            },
            error: function (result) {
                var errMesasgeObject = $.parseJSON(result.responseText);
                $("#outputName").text("Failed: " + errMesasgeObject.Message);
                $("#outputCurrency").text();
            }
        });
        return false;
    });
});