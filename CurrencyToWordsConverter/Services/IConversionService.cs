﻿using CurrencyToWordsConverter.Models;

namespace CurrencyToWordsConverter.Services
{
    public interface IConversionService
    {
        WordsCurrencyModel ConvertToWords(CurrencyModel model);
    }
}