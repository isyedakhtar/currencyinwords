﻿namespace CurrencyToWordsConverter.Services
{
    public interface ICurrencyFormatter
    {
        string FormatCurrency(string dollars, string cents);
    }
}