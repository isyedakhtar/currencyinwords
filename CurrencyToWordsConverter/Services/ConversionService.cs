﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CurrencyToWordsConverter.Models;
using CurrencyToWordsConverter.Repository;

namespace CurrencyToWordsConverter.Services
{
    /// <summary>
    /// Responsible for converting user intput to expected output. 
    /// </summary>
    public class ConversionService : IConversionService
    {
        private const long MaxSupportedNumber = 999999999999;
        private readonly ICurrencySeparator _wholeNumbersExtractor;
        private readonly IResourceRepository _resourceRepository;
        private readonly ICurrencyFormatter _currencyFormatter;
        private StringBuilder _wordsBuilder;

        public ConversionService(ICurrencySeparator wholeNumbersExtractor, IResourceRepository resourceRepository, ICurrencyFormatter currencyFormatter)
        {
            _wholeNumbersExtractor = wholeNumbersExtractor;
            _resourceRepository = resourceRepository;
            _currencyFormatter = currencyFormatter;
        }

        public WordsCurrencyModel ConvertToWords(CurrencyModel model)
        {
            ValidateNumber(model.Currency);

            //Get Dollars and Cents in a list
            var wholeNumbers = _wholeNumbersExtractor.SeparateDollarsAndCents(model.Currency).ToList();

            //Convert Dollars to words
            var dollars = ConvertWholeNumbers(wholeNumbers[0]);

            string cents = string.Empty;
            if (wholeNumbers.Count > 1)
            {
                //Convert cents to words
                cents = ConvertWholeNumbers(wholeNumbers[1]);
            }

            return new WordsCurrencyModel
            {
                CurrencyInWords = _currencyFormatter.FormatCurrency(dollars, cents),
                Name = model.Name
            };
        }             

        private void ValidateNumber(decimal num)
        {
            if (num > MaxSupportedNumber)
            {
                throw new NotSupportedException("Numbers greater than 999999999999 are not supported");
            }
        }

        private string ConvertWholeNumbers(Int64 num)
        {
            //reset builder
            _wordsBuilder = new StringBuilder();

            if (num == 0)
            {
                return ProcessZeroInput();
            }

            num = ProcessGreaterThanHundreds(num);
            ProcessHunderdsAndLess(num);
            return _wordsBuilder.ToString().Trim();
        }
        private string ProcessZeroInput()
        {
            _wordsBuilder.Append(_resourceRepository.GetText(0));
            return _wordsBuilder.ToString();
        }
        private Int64 ProcessHunderdsAndLess(Int64 num)
        {
            num = AddHundreds(num);
            var numberAfterTens = AddTens(num);
            AddUnitHyphen(num, numberAfterTens);
            AddUnits(numberAfterTens);
            return num;
        }
        private Int64 ProcessGreaterThanHundreds(Int64 num)
        {
            //Billion, Million and Thousand are processed here
            var greateThanHundredNumbers = new List<int> {1000000000, 1000000, 1000};

            foreach (var greateThanHundredNumber in greateThanHundredNumbers)
            {
                num = Process(num, greateThanHundredNumber);
            }

            return num;
        }
        private Int64 Process(Int64 num, Int64 greaterThanHumderd)
        {
            if (num > greaterThanHumderd - 1)
            {
                var baseScale = ((int)(num / greaterThanHumderd));
                ProcessHunderdsAndLess(baseScale);
                _wordsBuilder.Append($"{_resourceRepository.GetText(greaterThanHumderd)} ");
                num = num - (baseScale * greaterThanHumderd);
            }
            return num;
        }
        private void AddUnitHyphen(Int64 num, Int64 numberAfterTens)
        {
            if (num > 20 && numberAfterTens > 0)
            {
                _wordsBuilder.Append("-");
            }
        }
        private void AddUnits(Int64 num)
        {
            if (num > 0)
            {
                _wordsBuilder.Append($"{ _resourceRepository.GetText(num) } ");
            }
        }
        private Int64 AddTens(Int64 num)
        {
            if (num > 20)
            {
                var tens = ((int)(num / 10)) * 10;
                _wordsBuilder.Append($"{ _resourceRepository.GetText(tens) }");
                num = num - tens;
            }

            return num;
        }
        private Int64 AddHundreds(Int64 num)
        {
            if (num > 99)
            {
                var hundreds = ((int)(num / 100));
                _wordsBuilder.Append($"{ _resourceRepository.GetText(hundreds) } hundred ");
                num = num - (hundreds * 100);
            }
            return num;
        }        
    }
}