﻿using System.Text;

namespace CurrencyToWordsConverter.Services
{
    /// <summary>
    /// This class is responisble for formatting dollars and cents words e.g. four hundred dollars and 54 cents
    /// </summary>
    public class CurrencyFormatter : ICurrencyFormatter
    {
        public string FormatCurrency(string dollars, string cents)
        {
            var currencyBuilder = new StringBuilder();

            currencyBuilder.Append($"{ dollars } Dollars");

            if (!string.IsNullOrEmpty(cents))
            {
                currencyBuilder.Append(" and ");
                currencyBuilder.Append($"{ cents } Cents");
            }

            return currencyBuilder.ToString();
        }
    }
}