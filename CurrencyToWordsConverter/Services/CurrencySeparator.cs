﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CurrencyToWordsConverter.Services
{
    public class CurrencySeparator : ICurrencySeparator
    {
        /// <summary>
        /// Splits decimal currency into two whole numbers based on decimal(.) part. e.g. 98.52 will return two integers 98 and 52. First part represents dollars while other cents
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Int64> SeparateDollarsAndCents(decimal number)
        {
            return number.ToString(CultureInfo.InvariantCulture).Split('.').Select(s => Convert.ToInt64(s));
        }
    }
}