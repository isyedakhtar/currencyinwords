﻿using System;
using System.Collections.Generic;

namespace CurrencyToWordsConverter.Services
{
    public interface ICurrencySeparator
    {
        IEnumerable<Int64> SeparateDollarsAndCents(decimal number);
    }
}