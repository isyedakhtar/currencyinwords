﻿1. This project can be run hitting f5
2. Assumption is: this project is running at http://localhost:26297/
3. If for some reason you have to change th port 26297 to something else, please make sure to update it in ~/Scripts/App/CurrencyToWordsConverter.js file
4. A Sequence diagram is also there in the same directory as this file.
5. ResourceRepo added to make sure, if we want to do localization in future we should be able to do without change in core logic.
