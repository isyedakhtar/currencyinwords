﻿using System;

namespace CurrencyToWordsConverter.Repository
{
    public interface IResourceRepository
    {       
        string GetText(Int64 number);
    }
}