﻿using System.Web.Http;
using CurrencyToWordsConverter.Filters;

namespace CurrencyToWordsConverter.Controllers
{
    /// <summary>
    /// Provides a base for adding all cross cutting logic e.g. Model validation, exception logging etc. Might include some common logic for inheriting controllers
    /// </summary>
    [ModelStateValidator]
    public class BaseController : ApiController
    {
    }
}