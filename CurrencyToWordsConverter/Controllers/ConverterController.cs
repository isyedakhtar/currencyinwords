﻿using System.Web.Http;
using CurrencyToWordsConverter.Models;
using CurrencyToWordsConverter.Services;

namespace CurrencyToWordsConverter.Controllers
{
    /// <summary>
    /// Provides a POST endpoint to for handling frontend requests
    /// </summary>
    public class ConverterController : BaseController
    {
        private readonly IConversionService _conversionService;

        public ConverterController(IConversionService conversionService)
        {
            _conversionService = conversionService;
        }        


        // POST api/converter
        public IHttpActionResult Post(CurrencyModel model)
        {            
            return Ok(_conversionService.ConvertToWords(model));
        }
    }   
}
