﻿using System.Web.Mvc;

namespace CurrencyToWordsConverter.Controllers
{
    /// <summary>
    /// Only used to create a view.
    /// </summary>
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Currency To Words Converter";

            return View();
        }
    }
}
