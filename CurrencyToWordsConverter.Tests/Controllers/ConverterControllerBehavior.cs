﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using CurrencyToWordsConverter.Controllers;
using CurrencyToWordsConverter.Models;
using CurrencyToWordsConverter.Services;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace CurrencyToWordsConverter.Tests.Controllers
{
    [TestFixture]
    public class ConverterControllerBehavior
    {
        ConverterController _sut;

        private Mock<IConversionService> _mockedConversionService;

        [SetUp]
        public void SetUp()
        {            
            _mockedConversionService = new Mock<IConversionService>();
            _sut = new ConverterController(_mockedConversionService.Object)
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()                
            };
        }


        [Test]
        public void Post_Returns_AmountInWords()
        {
            //Arrange
            var currencyModel = new CurrencyModel
            {
                Name = "Syed Akhtar",
                Currency = 12
            };

            var wordsCurrencyModel = new WordsCurrencyModel
            {
                CurrencyInWords = "Twelve Dollars"
            };
            _mockedConversionService.Setup(s => s.ConvertToWords(It.IsAny<CurrencyModel>())).Returns(() => wordsCurrencyModel);

            //Action            
            var response = _sut.Post(currencyModel).ExecuteAsync(CancellationToken.None).Result;
            var result = JsonConvert.DeserializeObject<WordsCurrencyModel>(response.Content.ReadAsStringAsync().Result);

            //Assert
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            Assert.AreEqual(result.CurrencyInWords, "Twelve Dollars");
        }
    }
}
