﻿using System;
using CurrencyToWordsConverter.Models;
using CurrencyToWordsConverter.Repository;
using CurrencyToWordsConverter.Services;
using NUnit.Framework;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace CurrencyToWordsConverter.Tests.Services
{
    [TestFixture]
    public class ConversionServiceBehavior
    {
        private ConversionService _sut;

        [SetUp]
        public void SetUp()
        {
            _sut = new ConversionService(new CurrencySeparator(), new ResourceRepository(), new CurrencyFormatter());
        }

        [Test]
        public void ConvertToWords_Tens_AreConvertedSuccessfully()
        {
            //Arrange

            var currencyModel = new CurrencyModel
            {
                Name = "Syed Akhtar",
                Currency = new decimal(30)
            };

            //Action
            var result = _sut.ConvertToWords(currencyModel);

            //Assert
            var expectedCurrencyInWords = "thirty dollars";
            var actualCurrencyInWords = result.CurrencyInWords;           
            Assert.AreEqual(0, String.Compare(expectedCurrencyInWords, actualCurrencyInWords, StringComparison.InvariantCultureIgnoreCase));

            var expectedName = "Syed Akhtar";
            var actualName = result.Name;
            Assert.AreEqual(0, String.Compare(expectedName, actualName, StringComparison.InvariantCultureIgnoreCase));
        }

        [Test]
        public void ConvertToWords_Teens_AreConvertedSuccessfully()
        {
            //Arrange
            var currencyModel = new CurrencyModel
            {
                Name = "Syed Akhtar",
                Currency = new decimal(15)
            };

            //Action
            var result = _sut.ConvertToWords(currencyModel);

            //Assert
            var expectedCurrencyInWords = "Fifteen Dollars";
            var actualCurrencyInWords = result.CurrencyInWords;
            Assert.AreEqual(0, String.Compare(expectedCurrencyInWords, actualCurrencyInWords, StringComparison.InvariantCultureIgnoreCase));

            var expectedName = "Syed Akhtar";
            var actualName = result.Name;
            Assert.AreEqual(0, String.Compare(expectedName, actualName, StringComparison.InvariantCultureIgnoreCase));
        }

        [Test]
        public void ConvertToWords_Ones_AreConvertedSuccessfully()
        {
            //Arrange
            var currencyModel = new CurrencyModel
            {
                Name = "Syed Akhtar",
                Currency = new decimal(5)
            };

            //Action
            var result = _sut.ConvertToWords(currencyModel);

            //Assert
            var expected = "Five Dollars";
            var actual = result.CurrencyInWords;
            Assert.AreEqual(0, String.Compare(expected, actual, StringComparison.InvariantCultureIgnoreCase));
        }

        [Test]
        public void ConvertToWords_Hundreds_AreConvertedSuccessfully()
        {
            //Arrange
            var currencyModel = new CurrencyModel
            {
                Name = "Syed Akhtar",
                Currency = new decimal(512)
            };

            //Action
            var result = _sut.ConvertToWords(currencyModel);

            //Assert
            var actual = result.CurrencyInWords;
            var expected = "Five hundred twelve Dollars";
            Assert.AreEqual(0, String.Compare(expected, actual, StringComparison.InvariantCultureIgnoreCase));
        }

        [Test]
        public void ConvertToWords_Thousands_AreConvertedSuccessfully()
        {
            //Arrange
            var currencyModel = new CurrencyModel
            {
                Name = "Syed Akhtar",
                Currency = new decimal(5012)
            };

            //Action
            var result = _sut.ConvertToWords(currencyModel);

            //Assert
            var actual = result.CurrencyInWords;
            var expected = "Five thousand twelve Dollars";
            Assert.AreEqual(String.Compare(expected, actual, StringComparison.InvariantCultureIgnoreCase), 0);
        }

        [Test]
        public void ConvertToWords_Millions_AreConvertedSuccessfully()
        {
            //Arrange
            var currencyModel = new CurrencyModel
            {
                Name = "Syed Akhtar",
                Currency = new decimal(5112234)
            };

            //Action
            var result = _sut.ConvertToWords(currencyModel);

            //Assert
            var actual = result.CurrencyInWords;
            var expected = "five million one hundred twelve thousand two hundred thirty-four Dollars";
            Assert.AreEqual(0, String.Compare(expected, actual, StringComparison.InvariantCultureIgnoreCase));
        }

        [Test]
        public void ConvertToWords_Billions_AreConvertedSuccessfully()
        {
            //Arrange
            var currencyModel = new CurrencyModel
            {
                Name = "Syed Akhtar",
                Currency = new decimal(999999999999)
            };

            //Action
            var result = _sut.ConvertToWords(currencyModel);

            //Assert
            var actual = result.CurrencyInWords;
            var expected = "nine hundred ninety-nine billion nine hundred ninety-nine million nine hundred ninety-nine thousand nine hundred ninety-nine Dollars";

            Assert.AreEqual(String.Compare(expected, actual, StringComparison.InvariantCultureIgnoreCase), 0);
        }

        [Test]
        public void ConvertToWords_TensAndOnes_AddsHyphenBetweenTensAndOnes()
        {
            //Arrange
            var currencyModel = new CurrencyModel
            {
                Name = "Syed Akhtar",
                Currency = new decimal(524.34)
            };

            //Action
            var result = _sut.ConvertToWords(currencyModel);
            
            //Assert
            var actual = result.CurrencyInWords;
            var expected = "Five hundred twenty-four Dollars and thirty-four cents";
            Assert.AreEqual(String.Compare(expected, actual, StringComparison.InvariantCultureIgnoreCase), 0);
        }

        [Test]
        public void ConvertToWords_Trillion_ThrowsNotSupportedException()
        {
            //Arrange
            var model = new CurrencyModel
            {
                Name = "Syed Akhtar",
                Currency = new decimal(1111111111110)
            };

            //Action - Assert
            Assert.ThrowsException<NotSupportedException>(()=> _sut.ConvertToWords(model));
        }
    }
}