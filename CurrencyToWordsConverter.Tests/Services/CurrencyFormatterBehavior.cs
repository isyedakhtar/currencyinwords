﻿using System;
using CurrencyToWordsConverter.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace CurrencyToWordsConverter.Tests.Services
{
    [TestFixture]
    public class CurrencyFormatterBehavior
    {
        private CurrencyFormatter _sut;

        [SetUp]
        public void SetUp()
        {
            _sut = new CurrencyFormatter();
        }

        [Test]
        public void FormatCurrency_DollarsAndCents_FormatsCorrectly()
        {
            //Arrange
            var dollars = "Four hundred";
            var cents = "fifty-five";

            //Action
            var result = _sut.FormatCurrency(dollars, cents);

            //Assert
            Assert.AreEqual(string.Compare(result, "Four hundred Dollars And fifty-five Cents", StringComparison.CurrentCultureIgnoreCase), 0);
        }

        [Test]
        public void FormatCurrency_OnlyDollars_FormatsCorrectly()
        {
            //Arrange
            var cents = string.Empty;
            var dollars = "Four hundred";

            //Action
            var result = _sut.FormatCurrency(dollars, cents);

            //Assert
            Assert.AreEqual(string.Compare(result, "Four hundred Dollars", StringComparison.CurrentCultureIgnoreCase), 0);
        }
    }
}
