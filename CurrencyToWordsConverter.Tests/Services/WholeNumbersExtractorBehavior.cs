﻿using System.Linq;
using CurrencyToWordsConverter.Services;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace CurrencyToWordsConverter.Tests.Services
{
    [TestFixture]
    public class WholeNumbersExtractorBehavior
    {
        private CurrencySeparator _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new CurrencySeparator();
        }

        [Test]
        public void SeparateDollarsAndCents_DecimalNumber_SplitsIntoTwoWholeNumbers()
        {
            //Arrange
            decimal givenValur = new decimal(112.98);

            //Action
            var result = _sut.SeparateDollarsAndCents(givenValur).ToList();

            //Assert
            Assert.AreEqual(result.Count, 2);
        }

        [Test]
        public void SeparateDollarsAndCents_DecimalNumber_FirstNumerRepresentsDollars()
        {
            //Arrange
            decimal givenValur = new decimal(112.98);

            //Action
            var result = _sut.SeparateDollarsAndCents(givenValur).ToList();

            //Assert
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0], 112);
            Assert.AreEqual(result[1], 98);
        }

        [Test]
        public void SeparateDollarsAndCents_WholeNumber_ReturnsAsItIs()
        {
            //Arrange
            decimal givenValur = new decimal(112);

            //Action
            var result = _sut.SeparateDollarsAndCents(givenValur).ToList();

            //Assett
            Assert.AreEqual(result.Count, 1);
        }
    }
}
